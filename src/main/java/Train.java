import java.util.Observable;

/**
 *
 * Reprezentuje poruszający się pociąg.
 * Created by jan_w on 29.11.2017.
 */
public class Train extends Observable implements Runnable {

    private static int train_counter = 0;

    private int train_id = train_counter++;
    private boolean isCancelled = false;

    private int stationId;
    private boolean direction;  //true is increment, false is decrement;
    private TrainSchedule schedule;

    public Train(int stationId, boolean direction, TrainSchedule schedule) {
        this.stationId = stationId;
        this.direction = direction;
        this.schedule = schedule;
    }

    public int getTrain_id() {
        return train_id;
    }

    public int getStationId() {
        return stationId;
    }


    @Override
    public void run() {
        System.out.println("Pociąg rusza.");
        int schedule_counter = 0;

        // informuje o stacji 0 - ruszamy
        setChanged();
        notifyObservers();

        try {    // w stacji zawsze przesypia sekundę
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Pociąg rusza ze stacji 0.");

        while (schedule_counter < schedule.getSchedule().size()){
            // pobieram element rozkładu
            TrainScheduleRecord record = schedule.getSchedule().get(schedule_counter);

            System.out.println("Pociąg rusza ze stacji " + stationId);
            try {  // przesypiam tyle ile wskazuje rozkład - tyle czasu jedziemy
                Thread.sleep(record.getTime());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (direction){
                stationId--;
            }
            else {
                stationId++;
            }

            System.out.println("Pociąg dotarł do stacji " + stationId);
            // informuje o stacji N
            setChanged();
            notifyObservers("bla");

            if (!record.isSkip()){
                System.out.println("Pociąg zatrzymuje się na stacji " + stationId);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                System.out.println("Pociąg nie zatrzymał się na stacji.");
            }

            // jeśli pociąg został anulowany przerywamy pętle
            if (isCancelled){
                System.out.println("Pociąg został anulowany");
                // TODO: powiadomienie do wszystkich stacji o anulowaniu pociągu
                break;
            }
            schedule_counter++;
        }
        System.out.println("Pociąg zakończył bieg");

        // każdy pociąg porusza się po linii
        // każdy pociąg jeśli zatrzymuje sie na stacji, to czeka na niej dokładnie 1 sek.
        // każdy pociąg może być anulowany
        // każdy pociąg informuje headquarters o tym że dojechał na stację, nawet jeśli nie zatrzymuje się na niej (nie czeka 1s.)
    }

    public boolean isDirection(){
        return direction;
    }

    public void cancelTrain(){
        isCancelled = true;
    }
}
