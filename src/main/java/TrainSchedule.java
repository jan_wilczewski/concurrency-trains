import java.util.List;

/**
 * Created by jan_w on 29.11.2017.
 */
public class TrainSchedule {

    private List<TrainScheduleRecord> schedule;

    public TrainSchedule(List<TrainScheduleRecord> schedule) {
        this.schedule = schedule;
    }

    public List<TrainScheduleRecord> getSchedule() {
        return schedule;
    }
}
