import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jan_w on 29.11.2017.
 */
public class Headquarters implements Observer{

    public final static Boolean DIRECTION_WEJHEROWO = false;// wejherowo to stacja (stations.length -1)
    public final static Boolean DIRECTION_GDANSK = true;    // gdansk to stacja 0

    // ciuf ciuf
    private ExecutorService pociongi = Executors.newFixedThreadPool(10);

    private TrainSchedule scheduleFromGdansk;
    private TrainSchedule scheduleFromWejherowo;

    private List<Station> stationList;

    public Headquarters() {
        this.scheduleFromGdansk = new TrainSchedule(new LinkedList<TrainScheduleRecord>(
                Arrays.asList(
                        new TrainScheduleRecord(20000, false),
                        new TrainScheduleRecord(5000, false),
                        new TrainScheduleRecord(2000, false),
                        new TrainScheduleRecord(500, false),
                        new TrainScheduleRecord(2000, false)
                )));

        this.scheduleFromWejherowo = new TrainSchedule(new LinkedList<TrainScheduleRecord>(
                Arrays.asList(
                        new TrainScheduleRecord(2000, false),
                        new TrainScheduleRecord(500, false),
                        new TrainScheduleRecord(2000, false),
                        new TrainScheduleRecord(5000, false),
                        new TrainScheduleRecord(2000, false)
                )));

        // UWAGA! TRZEBA DODAĆ STACJE
        stationList = new LinkedList<Station>(
                Arrays.asList(
                        new Station(0, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(1, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(2, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(3, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(4, scheduleFromGdansk, scheduleFromWejherowo),
                        new Station(5, scheduleFromGdansk, scheduleFromWejherowo)
                ));
//        1)
//        for(Station s : stationList){
//            addObserver(s);
//        }

        // dodaj stacje aby obserwowały centralę lub sam/a zaimplementuj powiadamianie w metodzie update.
    }

    public void dispatchTrainFromGdansk() {
        Train t = new Train(0, DIRECTION_WEJHEROWO, scheduleFromGdansk);
        t.addObserver(this);
        pociongi.submit(t);
    }

    public void dispatchTrainFromWejherowo() {
        Train t = new Train(stationList.size() - 1, DIRECTION_GDANSK, scheduleFromWejherowo);
        t.addObserver(this);
        pociongi.submit(t);
    }

    @Override
    public void update(Observable o, Object arg) {

//        1)
//        setChanged();
//        notifyObservers("informacja");

//        2)
        if (o instanceof Train) {
            Train t = (Train) o;
            if (t.isDirection() == DIRECTION_WEJHEROWO) {
                for (int i = t.getStationId(); i < stationList.size(); i++) {
                    stationList.get(i).update(t, arg);
                }
            } else {
                for (int i = t.getStationId(); i >= 0; i--) {
                    stationList.get(i).update(t, arg);
                }
            }
        }
        // jest informowany o tym że jakiś pociąg dojechał do stacji i informuje o tym stacje.
        // to jakie stacje muszą być poinformowane wybieracie WY

    }

    public void printTrainInfoOf(int id) {
        stationList.get(id).print();
    }
}
