
import java.util.*;

/**
 * Reprezentuje pojedyncza stacje kolejowa.
 * <p>
 * DODATKOWE: zamień implementację station na JPanel (dodaj interfejs GUI) aby na okienku (headquarters)
 * wyswietalala sie lista wszystkich pociagow.

 * Created by jan_w on 29.11.2017.
 */
public class Station implements Observer {

    private int id;
    private TrainSchedule fromGdansk;   //grafik w jedna strone
    private TrainSchedule fromWejherowo;//grafik w druga strone

    private PriorityQueue<TrainInfo> listOfTrainsFromGdansk;
    private PriorityQueue<TrainInfo> listOfTrainsFromWladyslawowo;

    public Station(int id, TrainSchedule fromGdansk, TrainSchedule fromWejherowo) {
        this.id = id;
        this.fromGdansk = fromGdansk;
        this.fromWejherowo = fromWejherowo;

        this.listOfTrainsFromGdansk = new PriorityQueue(new TrainInfoComparator());
        this.listOfTrainsFromWladyslawowo = new PriorityQueue(new TrainInfoComparator());
    }

    public void update(Observable o, Object arg) {
        if (o instanceof Train) {
            Train t = (Train) o;
            System.out.println("stacja o id  " + id + " zostaje poinformowana o pociągu:" + t.getTrain_id());

            int obecna_stacja = t.getStationId();
            int nasza_stacja = id;
            int id_pociagu = t.getTrain_id();

            if (obecna_stacja == nasza_stacja) { // pociąg dotarł do naszej stacji i trzeba go usunąć kolejki priorytetowej (obu)

            } else {

                if (t.isDirection() == Headquarters.DIRECTION_WEJHEROWO) {
                    // jeśli jedzie w kierunku wejherowa to na stacji zero trzeba go dodać do kolejki priorytetowej
                    if (obecna_stacja == 0) {
                        // liczenie czasów dotarcia
                        long timeOfArrival = countTimeOfArrival(obecna_stacja, nasza_stacja, fromGdansk);
                        listOfTrainsFromGdansk.add(new TrainInfo(timeOfArrival, id_pociagu));
                    } else {
                        // trzeba go edytować
                    }
                } else {
                    // jeśli jedzie w kierunku gdańska to na stacji n-1 trzeba go dodać do kolejki priorytetowej
                    if (obecna_stacja == fromWejherowo.getSchedule().size()) {
                        // liczenie czasow dotarcia
                        long timeOfArrival = countTimeOfArrival(nasza_stacja, obecna_stacja, fromWejherowo);
                        listOfTrainsFromWladyslawowo.add(new TrainInfo(timeOfArrival, id_pociagu));
                    } else {
                        // trzeba go edytować
                    }
                    //
                }
            }
        }
        // lista wyświetla pociągi zgodnie z tylko tym, co ruszyło już ze stacji.
        // tutaj musi pojawić się "skomplikowana" logika oceny czasu dojazdu pociągu.
        // UWAGA! stacja nie wie o tym że pociąg nie zatrzymuje się na stacjach (innych niż ona sama)
        // UWAGA! (nie ma tego brać pod uwagę przy obliczaniu czasu dojazdu) więc pociagi beda zmienialy czasy dojazdow
        // Spróbuj użyć komparatora (liste listOfTrainsFromGdansk oraz listOfTrainsFromWladyslawowo zamien na priorityqueue)
    }

    public long countTimeOfArrival(int stacja_pociagu, int stacja_nasza, TrainSchedule rozklad) {
        long wynik = 0;
        for (int i = stacja_pociagu; i < stacja_nasza; i++) {
            wynik += rozklad.getSchedule().get(i).getTime();
        }
        return wynik;
    }

    public void print() {
        Iterator<TrainInfo> infos =listOfTrainsFromGdansk.iterator();
        while(infos.hasNext()){
            System.out.println(infos.next());
        }
    }

    public class TrainInfoComparator implements Comparator<TrainInfo> {
        public int compare(TrainInfo o1, TrainInfo o2) {
            return o1.getEstimatedTimeOfArrival() < o2.getEstimatedTimeOfArrival() ? -1 : 1;
        }
    }
}
